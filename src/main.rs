use std::collections::HashMap;
use std::io;
use rand::Rng;

static VOWELS: &'static [char] = &['a', 'e', 'i', 'o', 'u', 'y'];

enum CommandResult {
    Quit,
    Show,
    Help,
    List(String),
    Add { whom: String, which_dept: String },
}

fn make_random_vector(size: u32) -> Vec<u8> {
    let mut vector = vec![0u8; size as usize];
    rand::thread_rng()
        .try_fill(vector.as_mut_slice())
        .expect("oopsie");
    vector
}

fn average(vector: &[u8]) -> f64 {
    let mut total: f64 = 0.;
    for i in vector {
        total += *i as f64;
    }
    let length = vector.len() as f64;
    total / length
}

fn mode(vector: &[u8]) -> u8 {
    let mut counts = HashMap::new();
    let mut max_val = 0;
    let mut max_key: u8 = 0;
    for val in vector {
        let ct = counts.entry(val).or_insert(0);
        *ct += 1;
        if *ct > max_val {
            max_val = *ct;
            max_key = *val;
        }
    }
    max_key
}

fn median(vector: &[u8]) -> f64 {
    let mut ordered = vector.to_vec();
    ordered.sort();
    let middle = ordered.len() / 2;
    return match ordered.len() % 2 {
        1 => vector[middle + 1] as f64,
        0 => (vector[middle] as f64 + vector[middle + 1] as f64) / 2.,
        _ => unreachable!(),
    };
}

fn split_words(in_str: &str) -> Vec<&str> {
    let mut result = Vec::new();
    let mut i = 0;
    let mut j = 0;
    for c in in_str.chars() {
        if c == ' ' {
            result.push(&in_str[j..i]);
            j = i + 1;
        }
        i += 1;
    }
    result.push(&in_str[j..i]);
    return result;
}

fn piglatin_vowel(word: &str) -> String {
    format!("{}-hay", word)
}

fn piglatin_consonant(word: &str) -> String {
    format!("{}-{}ay", &word[1..], &word[..1])
}

fn piglatin_word(word: &str) -> String {
    if let Some(chr) = word.chars().next() {
        if VOWELS.contains(&chr.to_ascii_lowercase()) {
            piglatin_vowel(word)
        } else {
            piglatin_consonant(word)
        }
    } else {
        String::from(word)
    }
}

fn piglatin(in_str: &str) -> String {
    let mut result = String::new();
    let words = split_words(in_str);
    let mut first_word = true;
    for word in words {
        if !first_word {
            result.push(' ');
        }
        first_word = false;
        result.push_str(&piglatin_word(word));
    }
    return result;
}

fn read_command_1_word(command: &str) -> CommandResult {
    match &command.to_ascii_lowercase()[..] {
        "quit" => CommandResult::Quit,
        "show" => CommandResult::Show,
        _ => CommandResult::Help,
    }
}

fn read_command_2_words(command: &[&str]) -> CommandResult {
    if command[0] == "List" {
        CommandResult::List(String::from(command[1]))
    } else {
        CommandResult::Help
    }
}

fn read_command_4_words(command: &[&str]) -> CommandResult {
    if command[0] == "Add" && command[2] == "to" {
        CommandResult::Add { whom: String::from(command[1]), which_dept: String::from(command[3]) }
    } else {
        CommandResult::Help
    }
}

fn read_command(user_input: &str) -> CommandResult {
    let command = user_input.trim();
    match command.len() {
        0 => { return CommandResult::Help; }
        _ => (),
    }
    let words = split_words(command);
    return match words.len() {
        1 => { read_command_1_word(command) }
        2 => { read_command_2_words(&words) }
        4 => { read_command_4_words(&words) }
        _ => { CommandResult::Help }
    };
}

fn department() {
    let mut company: HashMap<String, Vec<String>> = HashMap::new();
    loop {
        let mut user_input = String::new();
        io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line");
        let cmd = read_command(&user_input);
        match cmd {
            CommandResult::Quit => { return; }
            CommandResult::Help => { println!("Type either 'Add machin to bidule', 'Show', or 'Quit'") }
            CommandResult::List(dept) => {
                match company.get(&dept) {
                    None => { println!("there's no department called {}", dept) }
                    Some(people) => {
                        display_people_in_department(&dept, people);
                    }
                }
            }
            CommandResult::Show => {
                for (dept, people) in &company {
                    display_people_in_department(dept, people)
                }
            }
            CommandResult::Add { whom, which_dept } => {
                let dept = company.entry(which_dept).or_insert(Vec::new());
                dept.push(whom);
            }
        }
    }
}

fn display_people_in_department(dept: &String, people: &Vec<String>) {
    println!("{} people in the {} department:", people.len(), dept);
    let mut sorted_people = people.to_vec();
    sorted_people.sort();
    for person in sorted_people {
        println!("    {}", person);
    }
}

fn main() {
    let vector = make_random_vector(32);
    println!("data: {:?}", vector);
    println!("average: {}", average(&vector));
    println!("mode: {}", mode(&vector));
    println!("median: {}", median(&vector));
    let planets = String::from("A beautiful coloured dentist elapsed first gown haha idiot");
    println!("{}", piglatin(&planets));
    department();
}
